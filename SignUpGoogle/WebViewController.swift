//
//  WebViewController.swift
//  SignUpGoogle
//
//  Created by RujuRaj on 6/25/17.
//  Copyright © 2017 SFSU. All rights reserved.
//

import UIKit
import WebKit
class WebViewController: UIViewController,UIWebViewDelegate {
    private var webView: UIWebView?
    static let GOOGLE_SIGNIN_URL="https://accounts.google.com/ServiceLogin"
    static let COOKIE_URL="https://www.google.com/maps/timeline"
//    static let KML_URL="https://www.google.com/maps/timeline/kml?authuser=0&pb="
    static let KML_URL="https://www.google.com/maps/timeline/kml?pb="
    static let COOKIE_KEY="cookies"
    override func loadView() {
        webView = UIWebView()
        view = webView
    }
    func clear()
    {
        UserDefaults.standard.removeObject(forKey: WebViewController.COOKIE_KEY)
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        clear()
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
        self.webView?.delegate=self
        if let cookie=checkForExistingCookie()
        {
            downloadKML(cookie: cookie)
        }
        else if let url = URL(string: WebViewController.GOOGLE_SIGNIN_URL)
        {
            let req = URLRequest(url: url)
            self.webView?.loadRequest(req)
        }
    }
    func getDate(_ day:Int,month:Int,year:Int,hour:Int,min:Int) -> Date!
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd H:mm"
        formatter.timeZone=TimeZone.autoupdatingCurrent
        let someDateTime = formatter.date(from: "\(year)/\(month)/\(day) \(hour):\(min)")
        return someDateTime
    }
    
    func getPbValue(dayBeforeToday:Int) -> String
    {
        // gets the kml for today
        // format:"!1m8!1m3!1i"+year+"!2i"+(month-1)+"!3i"+day+"!2m3!1i"+year+"!2i"+(month-1)+"!3i"+day
        let date = Date()
//        if let date = getDate(20, month: 1, year: 2017, hour: 0, min: 0)
//        {
            let calendar = Calendar.current
            let year = String(calendar.component(.year, from: date))
            let month = String(calendar.component(.month, from: date))
            let day = String(calendar.component(.day, from: date))
            let monthInt=Int(month)!-1
            let monthStr=String(monthInt)
            var str = "!1m8!1m3!1i"+year+"!2i"+monthStr+"!3i"+day+"!2m3!1i"+year+"!2i"+monthStr+"!3i"+day
            str=WebViewController.KML_URL+str
            return str
//        }
        return ""
    }
    func downloadKML(cookie:HTTPCookie)
    {
        let cookies=HTTPCookieStorage.shared.cookies(for: URL(string: WebViewController.COOKIE_URL)!)
        
        let headers=HTTPCookie.requestHeaderFields(with: cookies!)
        let kmlUrl=URL(string: getPbValue(dayBeforeToday: 2))
        
        let request  = NSMutableURLRequest(url: kmlUrl!)
        request.allHTTPHeaderFields=headers
        Downloader(callback: parseKML).download(request: request as URLRequest)
    }
    func parseKML(data:Data)
    {
        KMLParser(data: data).parseKML()
        // at the end
        print("at the end")
        DispatchQueue.main.async {
            // Update UI
            self.navigationController?.popViewController(animated: true)
        }
    }
    func checkForExistingCookie() -> HTTPCookie?
    {
        let prefs = UserDefaults.standard
        if let prefcookie = prefs.object(forKey: WebViewController.COOKIE_KEY)
        {
            let data=prefcookie as! Data
            
            return NSKeyedUnarchiver.unarchiveObject(with: data) as! HTTPCookie
        }
        else if let cookies = HTTPCookieStorage.shared.cookies
        {
            if(cookies.count>0)
            {
                for cookie in cookies
                {
                    if(cookie.domain.contains("google.com"))
                    {
                        let data=NSKeyedArchiver.archivedData(withRootObject: cookie)
                        prefs.set(data, forKey: WebViewController.COOKIE_KEY)
                        return cookie
                    }
                }
            }
        }
        return nil
    }
    var emailVal=""
    var profileName=""
    var cook:HTTPCookie!
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("finish")
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(request.url?.absoluteString)
        if(request.url?.absoluteString.contains("https://accounts.google.com/ManageAccount"))!
        {
            print("return false")
            downloadKML(cookie: cook)
            return false
        }
//        let prefs = UserDefaults.standard
//        if let prefcookie = prefs.object(forKey: WebViewController.COOKIE_KEY)
//        {
//            let data=prefcookie as! Data
//            let cook = NSKeyedUnarchiver.unarchiveObject(with: data) as! HTTPCookie
//            downloadKML(cookie: cook)
//            self.navigationController?.popViewController(animated: true)
//            return false
//        }
        if(navigationType == UIWebViewNavigationType.formSubmitted)
        {
            if(request.url?.absoluteString.contains("password"))!
            {
                emailVal=webView.stringByEvaluatingJavaScript(from: "document.getElementById('email-display').innerText")!
                if(emailVal == "")
                {
                    emailVal=webView.stringByEvaluatingJavaScript(from: "document.getElementById('Email').value")!
                }
                profileName=webView.stringByEvaluatingJavaScript(from: "document.getElementById('profile-name').innerText")!
                print(emailVal)
                print(profileName)
            }
        }
        else
        {
            emailVal=(self.webView?.stringByEvaluatingJavaScript(from: "document.getElementById('email-display').innerText"))!
            profileName=(self.webView?.stringByEvaluatingJavaScript(from: "document.getElementById('profile-name').innerText"))!
            print(emailVal)
            print(profileName)
        }
        if(self.emailVal != "" && self.profileName != "")
        {
            if let cookies = HTTPCookieStorage.shared.cookies
            {
                if(cookies.count>0)
                {
                    for cookie in cookies
                    {
                        if(cookie.domain.contains("google.com"))
                        {
                            let data=NSKeyedArchiver.archivedData(withRootObject: cookie)
                            let prefs = UserDefaults.standard
                            prefs.set(data, forKey: WebViewController.COOKIE_KEY)
                            cook=cookie
                        }
                    }
                }
            }
        }
        return true
    }
}
