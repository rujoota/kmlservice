//
//  HttpDownloader.swift
//  SignUpGoogle
//
//  Created by RujuRaj on 4/3/17.
//  Copyright © 2017 SFSU. All rights reserved.
//

import Foundation
class Downloader : NSObject, URLSessionDownloadDelegate {
    
    var url : URL?
    var callback:((Data)->())={_ in }
    // will be used to do whatever is needed once download is complete
    //var yourOwnObject : NSObject?
    
    init(callback:@escaping ((Data)->()))
    {
        self.callback = callback
    }
    
    //is called once the download is complete
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL)
    {
        //copy downloaded data to your documents directory with same names as source file
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let destinationUrl = documentsUrl!.appendingPathComponent(url!.lastPathComponent)
        let dataFromURL = NSData(contentsOf: location)
        dataFromURL?.write(to: destinationUrl, atomically: true)
        
        
        let kmlContents=String(data: dataFromURL as! Data, encoding: .utf8)
        print(kmlContents)
        self.callback(dataFromURL as! Data)
        //self.callback(kmlContents!)
        /*do {
            let mytext = try String(contentsOf: destinationUrl)
            print(mytext)
        }
        catch let e as NSError {
            print("file read error")
        }*/
    }
    
    //this is to track progress
    private func URLSession(session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64)
    {
    }
    
    // if there is an error during download this will be called
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
        if(error != nil)
        {
            //handle the error
            print("Download completed with error: \(error!.localizedDescription)");
        }
    }
    
    //method to be called to download
    func download(request: URLRequest)
    {
        self.url = request.url
        
        //download identifier can be customized. I used the "ulr.absoluteString"
        let sessionConfig = URLSessionConfiguration.background(withIdentifier: (url?.absoluteString)!)
        let session = Foundation.URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        let task = session.downloadTask(with: request)
        task.resume()
    }
}
