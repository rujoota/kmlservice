//
//  KMLParser.swift
//  SignUpGoogle
//
//  Created by RujuRaj on 4/9/17.
//  Copyright © 2017 SFSU. All rights reserved.
//

import Foundation
class KMLParser:NSObject,XMLParserDelegate
{
    let kmlData:Data
    var placeMarArr=[PlaceMark]()
    let tempPlaceMark=""
    var eName=""
    var placeMarkObj=PlaceMark()
    var timespan=NSMutableDictionary()
    var extendedData=[String:String]()
    init(data:Data)
    {
        self.kmlData=data
    }
    func parseKML()
    {
        let parser = XMLParser(data: kmlData)
            parser.delegate = self
            parser.parse()
        
    }
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        eName=elementName
        if elementName == "Placemark"
        {
            self.placeMarkObj=PlaceMark()
        }
        else if elementName=="extendedData"
        {
            extendedData=[String:String]()
        }
        else if elementName=="Data"
        {
            extendedData=attributeDict
        }
        else if elementName=="TimeSpan"
        {
            timespan=NSMutableDictionary()
        }
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Placemark"
        {
            placeMarArr.append(placeMarkObj)
        }
    }
    func parser(_ parser: XMLParser, foundCharacters string: String)
    {
        let data=string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
       
        if (!data.isEmpty) {
            if eName == "name" {
                self.placeMarkObj.name=data
            }
            else if eName == "address" {
                self.placeMarkObj.address=data
            }
            
            else if eName == "value" {
                if self.extendedData["name"]=="Category"
                {
                    self.placeMarkObj.type=data
                }
                //extendedData.setObject(data, forKey: "data" as NSCopying)
            }
            
            else if eName == "begin" {
                placeMarkObj.timeFrom=getDate(str: data)
                print("begin="+data)
                //timespan.setObject(data, forKey: "timespan" as NSCopying)
            }
            else if eName == "end" {
                placeMarkObj.timeFrom=getDate(str: data)
                print("end="+data)
                
            }
        }
    }
    func getDate(str:String)->Date
    {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        return dateFormatter.date(from: str)!
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        print(placeMarArr)
    }
}
